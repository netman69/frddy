CFLAGS = -O3 -std=c89 -Wall -pedantic
OBJS   = frddy.o test.o

.PHONY: all clean test

all: frddy_test

frddy_test: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

test: test_compare.txt test2_compare.txt
	diff -u test.txt test_compare.txt
	diff -u test2.txt test2_compare.txt

test_compare.txt: frddy_test test.json
	cat test.json | ./frddy_test > $@

test2_compare.txt: frddy_test
	# This checks the tests in test directory
	#   Except test34.json these come from http://www.json.org/JSON_checker/
	#   fail1.json succeeds but it should as RFC7159 allows string roots
	#   fail18.json succeeds but there is no maximum nesting depth in the spec
	#   fail4.json and fail9.json succeed because we do allow trailing commas
	./test.sh > $@

clean:
	rm -rf $(OBJS) frddy_test test_compare.txt test2_compare.txt
