### Frddy

**Frddy** is a minimalistic JSON parser written in C and optimized for small
 code size and memory usage.

The main features are:
- Small code size and low memory use.
- No dynamic memory allocation.
- No external dependencies.
- Ability to parse input byte-per-byte.
- No need to retain all the JSON data in memory to parse.
- Should accept any valid JSON input.
- Strict syntax except for allowing trailing commas in arrays and maps.
- Simple API inspired by Yxml.
- Intended for use with ASCII or UTF-8 input, `\uXXXX` escape sequences are
 translated into UTF-8.
- Numbers parsed to double precision float.
- No global variables.

#### Usage

The recommended way to use **Frddy** is to just copy the `frddy.c` and `frddy.h`
 files in your project folder and compile it in as you would your own C sources.
 There's a small example provided in the file **test.c** which can be compiled
 via the included Makefile, running `make` should give you the binary
 `frddy_test` which takes JSON data from stdin and outputs the contents of it in
 an arbitrary format to stdout.

To use it in your code start by including the header:
```c
#include "frddy.h"
```

Then you need a variable of the struct type `frddy_t` (hereafter referred to as
 the "state" or "state struct") where all the parser state will be stored and
 initilialize it with `frddy_init()`, and a variable to hold the return value of
 `frddy_parse()` which is described later:
```c
frddy_t f;
int ret;
frddy_init(&t);
```

Next you have to call `frddy_parse()` for every character of JSON data to be
 parsed:
```c
ret = frddy_parse(&f, character);
```

Because we don't know when a number ends before encountering a non-numeric
 character we sometimes end up getting the result of parsing only when the next
 character is encountered, so a trailing newline or other whitespace is
 mandatory.

What `frddy_parse()` does is update the state and return one of the following
 tokens:
- `FRDDY_NOP`: The character was parsed and the state variable was updated, but
 no JSON element was completely parsed yet, you can continue parsing. The rest
 of this explanations here will ignore this exists as a token.
- `FRDDY_ERR`: An error has occured because either the JSON data is invalid,
 arrays or maps are nested too deeply, the state is invalid, or I've screwed up
 and there is a bug. To parse more data after encountering this token first call
 `frddy_init()` on the state again. The size of the internal stack can be
 changed in `frddy.h` to allow for deeper nesting of arrays and maps.
- `FRDDY_STR`: A string has been parsed and the (possibly partial, see next
 entry) result is in the `f.val_str` member of the state struct, the result will
 be 0 terminated but the length can also be found in `f.val_str_len`.
- `FRDDY_STR_PART`: We are parsing a string that is too long for the string
 buffer in the state struct, we return a part of the string already (in the same
 way as described for `FRDDY_STR`). A call to `FRDDY_STR` will follow unless an
 error occurs first. The size of the string buffer can be changed in `frddy.h`.
- `FRDDY_NUM`: A number was parsed, the value can be found in the double
 `f.val_num`.
- `FRDDY_BOOL`: Either `true` or `false` was parsed, `f.val_bool` was set to 1
 or 0 accordingly.
- `FRDDY_NULL`: A literal `null` was parsed.
- `FRDDY_ARR_BEGIN`: An array begins, until `FRDDY_ARR_END` is returned or
 another array or map begins, the items parsed will be members of this array.
- `FRDDY_ARR_END`: An array that previously began has ended, this will only
 appear after a matching `FRDDY_ARR_BEGIN` was given.
- `FRDDY_MAP_BEGIN`: A map (sometimes referred to as object) begins, until
 `FRDDY_MAP_END`, `FRDDY_ARR_BEGIN` or an error occur alternatingly
 `FRDDY_STRING` keys and then matching values of whatever type is parsed will be
 returned. The variable `f.map_part` holds FRDDY_MAP_KEY at the time the key's
 string is returned, and FRDDY_MAP_VAL when the value is returned, for arrays
 and maps this only applies to `FRDDY_BEGIN_ARR` and `FRDDY_BEGIN_MAP`
 respectively. When not in a map this variable is set to `FRDDY_MAP_NONE` at
 all times. For the `FRDDY_NOP` token no guarantees are given about the value
 of `f.map_part`.
- `FRDDY_MAP_END`: A map that previously began has ended, this will only appear
 after a matching `FRDDY_MAP_BEGIN` was given.

The state struct also has the member `stack_pos` which keeps the position of the
 parser state stack. When the last token related to the root element is returned
 this variable will be 0, otherwise it will be a positive value. This is useful
 if you do not necessarily know where your JSON data ends. Other members of this
 struct not described here are probably not useful other than for debugging
 purposes.
