#ifndef __FRDDY_H__
#define __FRDDY_H__

#define FRDDY_STACK_DEPTH 64 /* Limits maximum amount of nesting. */
#define FRDDY_STRBUF_SIZE 512 /* Max length of strings parts. */

typedef struct frddy frddy_t;
typedef int (*frddy_state_t)(frddy_t *, char);

struct frddy {
	frddy_state_t state;
	frddy_state_t stack[FRDDY_STACK_DEPTH];
	unsigned int stack_pos;
	int next;
	char val_str[FRDDY_STRBUF_SIZE];
	unsigned int val_str_len, str_len, str_utf, str_utf_pos;
	double val_num, num_dec_mul;
	int num_sign, num_exp, num_exp_sign;
	int val_bool;
	char *kw_ptr;
	int map_part;
};

enum { /* Values for map_part. */
	FRDDY_MAP_NONE,
	FRDDY_MAP_KEY,
	FRDDY_MAP_VAL
};

enum { /* Return values. */
	FRDDY_ERR,
	FRDDY_NOP,
	FRDDY_STR,
	FRDDY_STR_PART,
	FRDDY_NUM,
	FRDDY_BOOL,
	FRDDY_NULL,
	FRDDY_ARR_BEGIN,
	FRDDY_ARR_END,
	FRDDY_MAP_BEGIN,
	FRDDY_MAP_END
};

extern void frddy_init(frddy_t *f);
extern int frddy_parse(frddy_t *f, char c);

#endif /* __FRDDY_H__ */
