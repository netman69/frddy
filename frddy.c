#include "frddy.h"

/* We always keep 4 characters of space in the string buffer for the maximum
 *   UTF-8 code size of 3 and a terminating zero to fit.
 */
#define FRDDY_STR_MAX (sizeof(f->val_str) - 4)

static int frddy_parse_elem(frddy_t *f, char c);
static int frddy_parse_str(frddy_t *f, char c);
static int frddy_parse_arr(frddy_t *f, char c);
static int frddy_parse_map(frddy_t *f, char c);

static int frddy_push(frddy_t *f, frddy_state_t st) {
	if (f->stack_pos >= sizeof(f->stack) / sizeof(f->stack[0]))
		return FRDDY_ERR;
	f->stack[f->stack_pos++] = f->state;
	f->state = st;
	return FRDDY_NOP;
}

static void frddy_pop(frddy_t *f) {
	f->state = f->stack[--f->stack_pos];
}

static int frddy_parse_str_part(frddy_t *f, char c) {
	if (f->str_len < FRDDY_STR_MAX)
		return FRDDY_NOP;
	f->val_str[f->str_len] = 0;
	f->val_str_len = f->str_len;
	f->str_len = 0;
	return FRDDY_STR_PART;
}

static int frddy_parse_str_utf(frddy_t *f, char c) {
	f->str_utf *= 0x10;
	++f->str_utf_pos;
	if (c >= '0' && c <= '9')
		f->str_utf += c - '0';
	else if (c >= 'A' && c <= 'F')
		f->str_utf += c - 'A' + 0x0A;
	else if (c >= 'a' && c <= 'f')
		f->str_utf += c - 'a' + 0x0A;
	else return FRDDY_ERR;
	if (f->str_utf_pos < 4)
		return FRDDY_NOP;
	if (f->str_utf < 0x80) {
		f->val_str[f->str_len++] = f->str_utf;
	} else if (f->str_utf < 0x0800) {
		f->val_str[f->str_len++] = (((f->str_utf >> 6) & 0x1F) | 0xC0);
		f->val_str[f->str_len++] = (((f->str_utf >> 0) & 0x3F) | 0x80);
	} else {
		f->val_str[f->str_len++] = (((f->str_utf >> 12) & 0x0F) | 0xE0);
		f->val_str[f->str_len++] = (((f->str_utf >>  6) & 0x3F) | 0x80);
		f->val_str[f->str_len++] = (((f->str_utf >>  0) & 0x3F) | 0x80);
	}
	f->state = frddy_parse_str;
	return frddy_parse_str_part(f, c);
}

static int frddy_parse_str_esc(frddy_t *f, char c) {
	f->state = frddy_parse_str;
	switch (c) {
		case '"':
		case '\\':
		case '/':
			f->val_str[f->str_len++] = c;
			return frddy_parse_str_part(f, c);
		case 'b':
			f->val_str[f->str_len++] = '\b';
			return frddy_parse_str_part(f, c);
		case 'f':
			f->val_str[f->str_len++] = '\f';
			return frddy_parse_str_part(f, c);
		case 'n':
			f->val_str[f->str_len++] = '\n';
			return frddy_parse_str_part(f, c);
		case 'r':
			f->val_str[f->str_len++] = '\r';
			return frddy_parse_str_part(f, c);
		case 't':
			f->val_str[f->str_len++] = '\t';
			return frddy_parse_str_part(f, c);
		case 'u':
			f->str_utf = 0;
			f->str_utf_pos = 0;
			f->state = frddy_parse_str_utf;
			return FRDDY_NOP;
	}
	return FRDDY_ERR;
}

static int frddy_parse_str(frddy_t *f, char c) {
	switch (c) {
		case '\\':
			f->state = frddy_parse_str_esc;
			return FRDDY_NOP;
		case '"':
			f->val_str[f->str_len] = 0;
			f->val_str_len = f->str_len;
			frddy_pop(f);
			return FRDDY_STR;
	}
	if ((unsigned) c < 0x20)
		return FRDDY_ERR;
	f->val_str[f->str_len++] = c;
	return frddy_parse_str_part(f, c);
}

/* This state is why we require a whitespace at the end. */
static int frddy_parse_next(frddy_t *f, char c) {
	int ret;
	frddy_pop(f);
	if ((ret = f->state(f, f->next)) != FRDDY_NOP) {
		frddy_push(f, frddy_parse_next);
		f->next = c;
		return ret;
	}
	f->next = -1;
	return f->state(f, c);
}

static int frddy_parse_num_end(frddy_t *f, char c) {
	f->val_num *= f->num_sign;
	/* Parsing a digit gives a chance to process f->next so it should be -1. */
	f->next = (unsigned char) c;
	f->state = frddy_parse_next;
	return FRDDY_NUM;
}

static int frddy_parse_num_exp(frddy_t *f, char c) {
	long double m;
	int i;
	if (c >= '0' && c <= '9') { /* Standard allows excess 0s. */
		f->num_exp = f->num_exp * 10 + (c - '0');
		return FRDDY_NOP;
	}
	for (i = 0, m = (f->num_exp_sign < 0) ? 0.1l : 10l; i < 16; ++i, m *= m)
		if (f->num_exp & (1 << i))
			f->val_num *= m;
	return frddy_parse_num_end(f, c);
}

static int frddy_parse_num_exp_first(frddy_t *f, char c) {
	if (c < '0' || c > '9')
		return FRDDY_ERR;
	f->num_exp = c - '0';
	f->state = frddy_parse_num_exp;
	return FRDDY_NOP;
}

static int frddy_parse_num_exp_sign(frddy_t *f, char c) {
	f->num_exp_sign = 1;
	switch (c) {
		case '-':
			f->num_exp_sign = -1;
		case '+':
			f->state = frddy_parse_num_exp_first;
			return FRDDY_NOP;
	}
	return frddy_parse_num_exp_first(f, c);
}

static int frddy_parse_num_dec(frddy_t *f, char c) {
	if (c >= '0' && c <= '9') {
		f->val_num += (f->num_dec_mul /= 10) * (c - '0');
		return FRDDY_NOP;
	}
	if ((c | 32) == 'e') {
		f->state = frddy_parse_num_exp_sign;
		return FRDDY_NOP;
	}
	return frddy_parse_num_end(f, c);
}

static int frddy_parse_num_dec_first(frddy_t *f, char c) {
	f->num_dec_mul = 0.1;
	if (c < '0' || c > '9')
		return FRDDY_ERR;
	f->val_num += f->num_dec_mul * (c - '0');
	f->state = frddy_parse_num_dec;
	return FRDDY_NOP;
}

static int frddy_parse_num(frddy_t *f, char c) {
	/* No prepending zeros allowed, hence we check f->val_num. */
	if (c >= '0' && c <= '9' && f->val_num != 0) {
		f->val_num = f->val_num * 10 + (c - '0');
		return FRDDY_NOP;
	}
	if (c == '.') {
		f->state = frddy_parse_num_dec_first;
		return FRDDY_NOP;
	}
	if ((c | 32) == 'e') {
		f->state = frddy_parse_num_exp_sign;
		return FRDDY_NOP;
	}
	return frddy_parse_num_end(f, c);
}

static int frddy_parse_num_first(frddy_t *f, char c) {
	if (c < '0' || c > '9')
		return FRDDY_ERR;
	f->val_num = c - '0';
	f->state = frddy_parse_num;
	return FRDDY_NOP;
}

static int frddy_parse_kw(frddy_t *f, char c) {
	if (*f->kw_ptr++ != c)
		return FRDDY_ERR;
	if (*f->kw_ptr != 0)
		return FRDDY_NOP;
	frddy_pop(f);
	return (f->val_bool == -1) ? FRDDY_NULL : FRDDY_BOOL;
}

static int frddy_parse_ws(frddy_t *f, char c) {
	if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
		return FRDDY_NOP;
	return FRDDY_ERR;
}

static int frddy_parse_arr_memb(frddy_t *f, char c) {
	f->map_part = FRDDY_MAP_NONE;
	if (c == ']') { /* Unlike the standard we accept trailing commas. */
		frddy_pop(f);
		return FRDDY_ARR_END;
	}
	if (frddy_parse_ws(f, c)) /* We depend on FRDDY_ERR being 0. */
		return FRDDY_NOP;
	f->state = frddy_parse_arr;
	return frddy_push(f, frddy_parse_elem) ? frddy_parse_elem(f, c) : FRDDY_ERR;
}

static int frddy_parse_arr(frddy_t *f, char c) {
	switch (c) {
		case ']':
			frddy_pop(f);
			return FRDDY_ARR_END;
		case ',':
			f->state = frddy_parse_arr_memb;
			return FRDDY_NOP;
	}
	return frddy_parse_ws(f, c);
}

static int frddy_parse_map_val(frddy_t *f, char c) {
	if (c != ':')
		return frddy_parse_ws(f, c);
	f->map_part = FRDDY_MAP_VAL;
	f->state = frddy_parse_map;
	return frddy_push(f, frddy_parse_elem);
}

static int frddy_parse_map_key(frddy_t *f, char c) {
	switch (c) {
		case '"':
			f->map_part = FRDDY_MAP_KEY;
			f->str_len = 0;
			f->state = frddy_parse_map_val;
			return frddy_push(f, frddy_parse_str);
		case '}': /* Unlike the standard we accept trailing commas. */
			f->map_part = FRDDY_MAP_NONE;
			frddy_pop(f);
			return FRDDY_MAP_END;
	}
	return frddy_parse_ws(f, c);
}

static int frddy_parse_map(frddy_t *f, char c) {
	switch (c) {
		case '}':
			f->map_part = FRDDY_MAP_NONE;
			frddy_pop(f);
			return FRDDY_MAP_END;
		case ',':
			f->state = frddy_parse_map_key;
			return FRDDY_NOP;
	}
	return frddy_parse_ws(f, c);
}

static int frddy_parse_elem(frddy_t *f, char c) {
	if (c >= '0' && c <= '9') {
		f->num_sign = 1;
		f->state = frddy_parse_num_first;
		return frddy_parse_num_first(f, c);
	}
	switch (c) {
		case '-':
			f->num_sign = -1;
			f->state = frddy_parse_num_first;
			return FRDDY_NOP;
		case '"':
			f->str_len = 0;
			f->state = frddy_parse_str;
			return FRDDY_NOP;
		case 't':
			f->kw_ptr = "rue";
			f->val_bool = 1;
			f->state = frddy_parse_kw;
			return FRDDY_NOP;
		case 'f':
			f->kw_ptr = "alse";
			f->val_bool = 0;
			f->state = frddy_parse_kw;
			return FRDDY_NOP;
		case 'n':
			f->kw_ptr = "ull";
			f->val_bool = -1;
			f->state = frddy_parse_kw;
			return FRDDY_NOP;
		case '[':
			f->state = frddy_parse_arr_memb;
			return FRDDY_ARR_BEGIN;
		case '{':
			f->state = frddy_parse_map_key;
			return FRDDY_MAP_BEGIN;
	}
	return frddy_parse_ws(f, c);
}

void frddy_init(frddy_t *f) {
	f->state = frddy_parse_elem;
	f->stack[0] = frddy_parse_ws;
	f->stack_pos = 1;
	f->next = -1;
	f->map_part = FRDDY_MAP_NONE;
}

int frddy_parse(frddy_t *f, char c) {
	return f->state(f, c);
}
