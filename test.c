#include "frddy.h"
#include <unistd.h>
#include <stdio.h>

static int parse(frddy_t *f, char c) {
	int ret = frddy_parse(f, c);
	if (ret == FRDDY_NOP)
		return 1;
	if (ret == FRDDY_ERR) {
		printf("err %d\n", f->stack_pos);
		return 0;
	}
	if (f->map_part == FRDDY_MAP_KEY)
		printf("key: ");
	if (f->map_part == FRDDY_MAP_VAL)
		printf("val: ");
	switch (ret) {
		case FRDDY_STR_PART:
			printf("string part \"%s\"\n", f->val_str);
			break;
		case FRDDY_STR:
			printf("string \"%s\"\n", f->val_str);
			break;
		case FRDDY_NUM:
			printf("number %f\n", f->val_num);
			break;
		case FRDDY_BOOL:
			printf("bool %s\n", f->val_bool ? "true" : "false");
			break;
		case FRDDY_NULL:
			printf("null\n");
			break;
		case FRDDY_ARR_BEGIN:
			printf("array begin\n");
			break;
		case FRDDY_ARR_END:
			printf("array end\n");
			break;
		case FRDDY_MAP_BEGIN:
			printf("map begin\n");
			break;
		case FRDDY_MAP_END:
			printf("map end\n");
			break;
		default:
			printf("unknown return value %d\n", ret);
	}
	if (f->stack_pos == 0)
		printf("end of root object\n");
	return 1;
}

int main(int argc, char *argv[]) {
	frddy_t f;
	char buf[4096];
	int len, i;
	frddy_init(&f);
	while (1) {
		if ((len = read(STDIN_FILENO, buf, sizeof(buf))) <= 0)
			break;
		for (i = 0; i < len; ++i)
			if (!parse(&f, buf[i]))
				return 1;
	}
	if (!parse(&f, '\n'))
		return 1;
	if (f.stack_pos != 0) {
		printf("partial json %d\n", f.stack_pos);
		return 1;
	}
	return 0;
}
